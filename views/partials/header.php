<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>web programing</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.8/css/dataTables.bootstrap5.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<div class="container row">
<body>
    <header class="d-flex justify-content py-3">
        <ul class="nav nav-pills">
            <li class="nav-item"><a href="/" class="nav-link" aria-current="page">
                    Beranda
                </a>
            </li>
            <li class="nav-item"><a href="dosen" class="nav-link">
                    Dosen
                </a>
            </li>
            <li class="nav-item"><a href="mahasiswa" class="nav-link">
                    Mahasiswa
                </a>
            </li>
            <li class="nav-item"><a href="/prodi?act=list" class="nav-link">
                    Prodi
                </a>
            </li>
            <li class="nav-item"><a href="/tempat_magang" class="nav-link">
                    Tempat Magang
                </a>
            </li>
            <li class="nav-item"><a href="/jadwal_magang" class="nav-link">
                    Jadwal Magang
                </a>
            </li>
        </ul>
    </header>
    </div>
    <main>
        <div class="container">
            