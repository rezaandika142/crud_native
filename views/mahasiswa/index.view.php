<?php require 'views/partials/header.php' ?>

<div class="content py-3">
    <h1>Mahasiswa</h1>
</div>
<div class="mb-3 d-flex justify-content-between">
    <a href="/mahasiswa?act=create" class="btn btn-primary">Tambah Mahasiwa</a>
    <form class="d-flex" role="search">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
    </form>
</div>
<table class="table table-bordered align-middle">
    <thead>
        <tr>
            <th>NIM</th>
            <th>Nama</th>
            <th>Prodi</th>
            <th>Semester</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dataMahasiswa as $mahasiswa) : ?>
        <tr>
            <td><?php echo $mahasiswa['nim'] ?></td>
            <td><?php echo $mahasiswa['nama'] ?></td>
            <td><?php echo $mahasiswa['nama_prodi'] ?></td>
            <td><?php echo $mahasiswa['semester'] ?></td>
            <td class="text-center d-flex gap-2 justify-content-center">
                <a href="/mahasiswa?act=edit&id=<?=$mahasiswa['id'] ?>" class="btn btn-primary">Edit</a>
                <form action="/mahasiswa?act=del&id=<?=$mahasiswa['id'] ?>" method="post"
                    onsubmit="return confirm('Yakin akan di hapus?')">
                    <button class="btn btn-danger">Delete</a>
                </form>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<ul class="pagination">
    <li class="page-item <?php echo ($current_page <= 1) ? 'disabled' : ''; ?>">
        <a class="page-link" href="?page=<?php echo ($current_page - 1); ?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
        </a>
    </li>
    <?php for ($i = 1; $i <= $total_halaman; $i++) : ?>
        <li class="page-item <?php echo ($current_page == $i) ? 'active' : ''; ?>">
            <a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a>
        </li>
    <?php endfor; ?>
    <li class="page-item <?php echo ($current_page >= $total_halaman) ? 'disabled' : ''; ?>">
        <a class="page-link" href="?page=<?php echo ($current_page + 1); ?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
        </a>
    </li>
  </ul>
<?php require 'views/partials/footer.php' ?>