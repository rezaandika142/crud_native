<?php require 'views/partials/header.php' ?>

<div class="content py-5">
    <h1><?= $title ?></h1>
</div>

<div class="mb-3 d-flex gap-2">

</div>

<form class="card mb-5 w-50" method="post" action="">
    <div class="card-header">
        <h3 class="card-title">Edit Data</h3>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <label class="form-label" for="">NIM</label>
            <input type="text" class="form-control" name="nim" value="<?= $mahasiswa['nim'] ?>">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Nama</label>
            <input type="text" class="form-control" name="nama"  value="<?= $mahasiswa['nama'] ?>">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Prodi</label>
            <select name="prodi_id" id="prodi_id" class="form-select">
                <option value="">- Pilih -</option>
                <?php foreach ($list_prodi as $prodi) : ?>
                    <?php if ($prodi['id'] == $mahasiswa['prodi_id']) : ?>
                        <option value="<?= $prodi['id'] ?>" selected><?= $prodi['nama'] ?></option>
                    <?php else : ?>
                        <option value="<?= $prodi['id'] ?>"><?= $prodi['nama'] ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Semester</label>
            <input type="text" class="form-control" name="semester" value="<?= $mahasiswa['semester'] ?>">
        </div>
    </div>
    <div class="card-footer d-flex gap-2">
        <a href="/mahasiswa" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary" name="edit">Simpan</button>
    </div>
</form>

<?php require 'views/partials/footer.php' ?>