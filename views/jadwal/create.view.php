<?php require 'views/partials/header.php' ?>

<div class="content py-5">
    <h1><?= $title ?></h1>
</div>

<div class="mb-3 d-flex gap-2">

</div>

<form class="card mb-5 w-50" method="post" action="">
    <div class="card-header">
        <h3 class="card-title">Tambah Data</h3>
    </div>
    <div class="card-body">
    <div class="mb-3">
            <label class="form-label" for="">Nama Mahasiswa</label>
            <select name="mahasiswa_id" id="mahasiswa_id" class="form-select">
                <?php foreach ($list_mahasiswa as $mahasiswa) : ?>
                    <option value="<?= $mahasiswa['id'] ?>"><?= $mahasiswa['nama'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Prodi</label>
            <input type="text" class="form-control" name="prodi_id" id="prodi_id" disabled readonly>
            <?php foreach ($list_mahasiswa as $p) : ?>
                <option value="<?= $p['id'] ?>"><?= $p['prodi_id'] ?></option>
            <?php endforeach; ?>
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Tanggal Awal</label>
            <input type="date" class="form-control" name="tanggal_awal" id="tanggal_awal">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Tanggal Akhir</label>
            <input type="date" class="form-control" name="tanggal_akhir" id="tanggal_akhir">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Tempat Magang</label>
            <select name="tempat_id" id="tempat_id" class="form-select">
                <?php foreach ($dataTempatMagang as $tempat) : ?>
                    <option value="<?= $tempat['id'] ?>"><?= $tempat['namaTempat'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Dosen Pembimbing</label>
            <select name="dosen_id" id="dosen_id" class="form-select">
                <?php foreach ($dataDosen as $dosen) : ?>
                    <option value="<?= $dosen['id'] ?>"> <?= $dosen['gelarDepan'] ?> <?= $dosen['nama'] ?> <?= $dosen['gelarBelakang'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="card-footer d-flex gap-2">
        <a href="/mahasiswa" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary">Simpan</button>
    </div>
</form>

<?php require 'views/partials/footer.php' ?>