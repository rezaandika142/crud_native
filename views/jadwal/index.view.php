<?php require 'views/partials/header.php' ?>

<div class="content py-3">
    <h1>Jadwal Magang</h1>
</div>
<div class="mb-3 d-flex justify-content-between">
    <a href="/jadwal_magang?act=create" class="btn btn-primary">Tambah Jadwal Magang</a>
    <form class="d-flex" role="search">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
    </form>
</div>
<table id="dataTable" class="table table-bordered align-middle">
    <thead>
        <tr>
            <th>Nama Mahasiswa</th>
            <th>Program Studi</th>
            <th>Jadwal Magang</th>
            <th>Tempat Magang</th>
            <th>Dosen Pembimbing</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dataMagang as $Magang) : ?>
        <tr>
            <td><?php echo $Magang['nama_mahasiswa'] ?></td>
            <td><?php echo $Magang['nama_prodi'] ?></td>
            <td><?php
                    $tanggal_awal = new DateTime($Magang['tanggal_awal']);
                    $tanggal_akhir = new DateTime($Magang['tanggal_akhir']);
                    echo $tanggal_awal->format('d F Y') . ' - ' . $tanggal_akhir->format('d F Y');
            ?></td>
            <td><?php echo $Magang['nama_tempat'] ?></td>
            <td><?php echo $Magang['gelar_depan'] . ' ' . $Magang['nama_dosen'] . ' ' . $Magang['gelar_belakang']; ?></td>
            <td class="text-center d-flex gap-2 justify-content-center">
                <a href="/tempat_magang?act=edit&id=<?=$Magang['id'] ?>" class="btn btn-primary">Edit</a>
                <form action="/tempat_magang?act=del&id=<?=$Magang['id'] ?>" method="post"
                    onsubmit="return confirm('Yakin akan di hapus?')">
                    <button class="btn btn-danger">Delete</a>
                </form>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<ul class="pagination">
    <li class="page-item <?php echo ($current_page <= 1) ? 'disabled' : ''; ?>">
        <a class="page-link" href="?page=<?php echo ($current_page - 1); ?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
        </a>
    </li>
    <?php for ($i = 1; $i <= $total_halaman; $i++) : ?>
        <li class="page-item <?php echo ($current_page == $i) ? 'active' : ''; ?>">
            <a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a>
        </li>
    <?php endfor; ?>
    <li class="page-item <?php echo ($current_page >= $total_halaman) ? 'disabled' : ''; ?>">
        <a class="page-link" href="?page=<?php echo ($current_page + 1); ?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
        </a>
    </li>
  </ul>
<script>
    new DataTable('#dataTable');
</script>
<?php require 'views/partials/footer.php' ?>