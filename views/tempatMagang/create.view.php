<?php require 'views/partials/header.php' ?>

<div class="content py-5">
    <h1><?= $title ?></h1>
</div>

<div class="mb-3 d-flex gap-2">

</div>

<form class="card mb-5 w-50" method="post" action="">
    <div class="card-header">
        <h3 class="card-title">Tambah Data</h3>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <label class="form-label" for="">Nama Tempat</label>
            <input type="text" class="form-control" name="namaTempat">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Alamat</label>
            <input type="text" class="form-control" name="alamat">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Kota/Kabupaten</label>
            <input type="text" class="form-control" name="kotaKab">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Provinsi</label>
            <input type="text" class="form-control" name="provinsi">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Telepon</label>
            <input type="text" class="form-control" name="telepon">
        </div>
    </div>
    <div class="card-footer d-flex gap-2">
        <a href="/mahasiswa" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary">Simpan</button>
    </div>
</form>

<?php require 'views/partials/footer.php' ?>