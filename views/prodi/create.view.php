<?php require 'views/partials/header.php' ?>

<div class="content py-5">
    <h1><?= $title ?></h1>
</div>

<div class="mb-3 d-flex gap-2">

</div>
<form class="card mb-5 w-50" method="post" action="">
    <div class="card-header">
        <h3 class="card-title">Tambah Data</h3>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <label class="form-label" for="">Nama</label>
            <input type="text" class="form-control" name="nama">
        </div>
    </div>
    <div class="card-footer d-flex gap-2">
        <a href="/prodi?act=list" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary">Simpan</button>
    </div>
</form>

<?php require 'views/partials/footer.php' ?>