<?php require 'partials/header.php' ?>
<div class="content py-3">
    <h1>BERANDA</h1>
    <?php if (isset($_SESSION['sudah_login'])) : ?>
        <div class="card w-50 mb-5">
            <div class="card-body">
               <h3>Halo Selamat Datang, Unimus! </h3>
                <form action="/logout" method="post">
                    <button class="btn btn-primary">Logout</button>
                </form>
            </div>
        </div>
    <?php else : ?>
        <div class="card w-50">
            <div class="card-header">
                <h3 class="card-title">Login</h3>
            </div>
            <div class="card-body">
                <form action="/login" method="post">
                    <div class="row g-2 mb-2">
                        <label class="col-md-4 col-form-label" for="">Username</label>
                        <div class="col-md-7">
                            <input type="text" class="form-control" name="username">
                        </div>
                    </div>
                    <div class="row g-2 mb-2">
                        <label class="col-md-4 col-form-label" for="">Password</label>
                        <div class="col-md-7">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <button class="btn btn-primary">Login</button>

                </form>
            </div>
        </div>
    <?php endif; ?>
</div>



<?php require 'partials/footer.php' ?>