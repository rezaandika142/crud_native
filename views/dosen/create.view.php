<?php require 'views/partials/header.php' ?>

<div class="content py-5">
    <h1><?= $title ?></h1>
</div>

<div class="mb-3 d-flex gap-2">

</div>

<form class="card mb-5 w-50" method="post" action="">
    <div class="card-header">
        <h3 class="card-title">Tambah Data</h3>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <label class="form-label" for="">NIK</label>
            <input type="text" class="form-control" name="nik">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Nama</label>
            <input type="text" class="form-control" name="nama">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Gelar Depan</label>
            <input type="text" class="form-control" name="gelarDepan">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Gelar Belakang</label>
            <input type="text" class="form-control" name="gelarBelakang">
        </div>
        <div class="mb-3">
            <label class="form-label" for="">Prodi</label>
            <select name="prodi_id" id="prodi_id" class="form-select">
                <?php foreach ($list_prodi as $prodi) : ?>
                    <option value="<?= $prodi['id'] ?>"><?= $prodi['nama'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="card-footer d-flex gap-2">
        <a href="/dosen" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary">Simpan</button>
    </div>
</form>

<?php require 'views/partials/footer.php' ?>