<?php require 'views/partials/header.php' ?>

<div class="content py-3">
    <h1>Dosen</h1>

</div>
<div class="mb-3 d-flex justify-content-between">
    <a href="/dosen?act=create" class="btn btn-primary">Tambah Dosen</a>
    <form class="d-flex" role="search">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
    </form>
</div>

<table id="tableDosen" class="display table table-bordered align-middle" style="width:100%;">
    <thead>
        <tr>
            <th>NIK</th>
            <th>Nama</th>
            <th>Gelar Depan</th>
            <th>Gelar Belakang</th>
            <th>Prodi</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($dataDosen as $dosen) : ?>
        <tr>
            <td><?php echo $dosen['nik'] ?></td>
            <td><?php echo $dosen['nama'] ?></td>
            <td><?php echo $dosen['gelarDepan'] ?></td>
            <td><?php echo $dosen['gelarBelakang'] ?></td>
            <td><?php echo $dosen['nama_prodi'] ?></td>
            <td class="text-center d-flex gap-2 justify-content-center">
                <a href="/dosen?act=edit&id=<?=$dosen['id'] ?>" class="btn btn-primary">Edit</a>
                <form action="/dosen?act=del&id=<?=$dosen['id'] ?>" method="post"
                    onsubmit="return confirm('Yakin akan di hapus?')">
                    <button class="btn btn-danger">Delete</a>
                </form>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<ul class="pagination">
    <li class="page-item <?php echo ($current_page <= 1) ? 'disabled' : ''; ?>">
        <a class="page-link" href="?page=<?php echo ($current_page - 1); ?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
        </a>
    </li>
    <?php for ($i = 1; $i <= $total_halaman; $i++) : ?>
        <li class="page-item <?php echo ($current_page == $i) ? 'active' : ''; ?>">
            <a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a>
        </li>
    <?php endfor; ?>
    <li class="page-item <?php echo ($current_page >= $total_halaman) ? 'disabled' : ''; ?>">
        <a class="page-link" href="?page=<?php echo ($current_page + 1); ?>" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
        </a>
    </li>
  </ul>
<?php require 'views/partials/footer.php' ?>