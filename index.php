<?php
session_start();

$uri = parse_url($_SERVER['REQUEST_URI'])['path'];
$routes = [
    '/' => 'controllers/index.php',
    '/dosen' => 'controllers/dosen.php',
    '/kontak' => 'controllers/kontak.php',
    '/mahasiswa' => 'controllers/mahasiswa.php',
    '/prodi' => 'controllers/prodi.php',
    '/tempat_magang' => 'controllers/tempat_magang.php',
    '/jadwal_magang' => 'controllers/jadwal_magang.php',
    '/login' => 'controllers/login.php',
    '/logout' => 'controllers/logout.php',
];

if (array_key_exists($uri, $routes)) {
    require_once $routes[$uri];
}

die();