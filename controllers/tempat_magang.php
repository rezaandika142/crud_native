<?php

$connection = new PDO("mysql:host=localhost;port=3306;dbname=magang;user=root;charset=utf8mb4;");

if(!isset($_SESSION['sudah_login'])) {
    header('Location: /');
}

$act = '';

$data_per_halaman = 3;

    $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
    
    $batas_awal = ($current_page - 1) * $data_per_halaman;
    $batas_akhir = $batas_awal + $data_per_halaman - 1;
    $query = $connection->prepare("SELECT * FROM prodi LIMIT $batas_awal, $data_per_halaman");
    $query->execute();
    $data_dosen = $query->fetchAll(PDO::FETCH_ASSOC);
    $query_total = $connection->query("SELECT COUNT(*) AS total_data FROM prodi");
    $result_total = $query_total->fetch(PDO::FETCH_ASSOC);
    $total_data = $result_total['total_data'];
    $total_halaman = ceil($total_data / $data_per_halaman);
if ($act == 'del') {
    $id = $_GET['id'];
    $query = $connection->prepare("DELETE FROM tempat_magang WHERE id = ?");
    $query->execute([$id]);

    header('location: /tempat_magang');

}elseif (isset($_GET['act']) && $_GET['act'] == 'create') {
    // menampilkan form input
    $title = "Create Tempat Magang";
    
    if ($_POST) {
        $input = $_POST;
        $query = $connection->prepare("INSERT INTO tempat_magang (namaTempat, alamat, kotaKab, provinsi, telepon) VALUES (?,?,?,?,?)");
        $query->execute([$input["namaTempat"], $input["alamat"], $input["kotaKab"], $input["provinsi"], $input["telepon"]]);

        header('location: /tempat_magang');
    }
    require './views/tempatMagang/create.view.php';
}elseif (isset($_GET['act']) && $_GET['act'] == 'store') {
    // menyimpan data
    // di redirect ke list / index
    
}elseif (isset($_GET['act']) && $_GET['act'] == 'edit') {
    // menampilkan form edit
    if (!isset($_POST['edit'])) {
    $title = 'Edit Tempat Magang';
        $id = $_GET['id'];
        $query = $connection->prepare('SELECT * FROM tempat_magang WHERE id = ?');
        $query->execute([$id]);
        $tempatMagang = $query->fetch(PDO::FETCH_ASSOC);

        require './views/tempatMagang/edit.view.php';
    }else {
        $id = $_GET['id'];
        $input = $_POST;
        $query = $connection->prepare("UPDATE tempat_magang SET namaTempat = ?, alamat = ?, kotaKab = ?, provinsi = ?, telepon = ? WHERE id = ?");
        $query->execute([$input["namaTempat"], $input["alamat"], $input["kotaKab"], $input["provinsi"], $input["telepon"], $id]);
        header('location: /tempat_magang');
    }

}else {
    $title = "Data Tempat Magang";

    // Menampilkan list Tempat Magang
    if (isset($_GET['keyword'])) {
        $keyword = $_GET['keyword'];
        $query = $connection->prepare('SELECT * FROM tempat_magang.* WHERE nama LIKE "%' . $keyword . '%"');
        $query->execute();
        $dataTempatMagang = $query->fetchAll(PDO::FETCH_ASSOC);
    } else {
        $query = $connection->prepare("SELECT * FROM tempat_magang");
        $query->execute();
        $dataTempatMagang = $query->fetchAll(PDO::FETCH_ASSOC);
    }

    require './views/tempatMagang/index.view.php';
}
