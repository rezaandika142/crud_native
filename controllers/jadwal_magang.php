<?php

$connection = new PDO("mysql:host=localhost;port=3306;dbname=magang;user=root;charset=utf8mb4;");

if(!isset($_SESSION['sudah_login'])) {
    header('Location: /');
}

$act = '';
$dataMagang = $connection->query('SELECT * FROM jadwal_magang');
if (isset($_GET['act'])) {
    $act = $_GET['act'];
}


$data_per_halaman = 3;

$current_page = isset($_GET['page']) ? $_GET['page'] : 1;

$batas_awal = ($current_page - 1) * $data_per_halaman;
$batas_akhir = $batas_awal + $data_per_halaman - 1;
$query = $connection->prepare("SELECT * FROM mahasiswa LIMIT $batas_awal, $data_per_halaman");
$query->execute();
$data_dosen = $query->fetchAll(PDO::FETCH_ASSOC);
$query_total = $connection->query("SELECT COUNT(*) AS total_data FROM mahasiswa");
$result_total = $query_total->fetch(PDO::FETCH_ASSOC);
$total_data = $result_total['total_data'];
$total_halaman = ceil($total_data / $data_per_halaman);

if ($act == 'del') {
    $id = $_GET['id'];
    $query = $connection->prepare("DELETE FROM jadwal_magang WHERE id = ?");
    $query->execute([$id]);

    header('location: /jadwal_magang');

}elseif (isset($_GET['act']) && $_GET['act'] == 'create') {
    // menampilkan form input
    $title = "Create Jadwal Magang";
    $list_mahasiswa = $connection->query('SELECT * FROM mahasiswa');
    $dataTempatMagang = $connection->query('SELECT * FROM tempat_magang');
    $dataDosen = $connection->query('SELECT * FROM dosen');
    $list_prodi = $connection->query('SELECT * FROM prodi');
    if ($_POST) {
        $input = $_POST;
        $query = $connection->prepare("INSERT INTO jadwal_magang (mahasiswa_id, prodi_mhs, tanggal_awal, tanggal_akhir, tempat_id, dosen_id) VALUES (?,?,?,?,?,?)");
        $query->execute([$input["mahasiswa_id"], $input["prodi_mhs"], $input["tanggal_awal"], $input["tanggal_akhir"], $input["tempat_id"], $input["dosen_id"]]);

        header('location: /jadwal_magang');
    }
    require './views/jadwal/create.view.php';
}elseif (isset($_GET['act']) && $_GET['act'] == 'store') {
    // menyimpan data
    // di redirect ke list / index
    
}elseif (isset($_GET['act']) && $_GET['act'] == 'edit') {
    // menampilkan form edit
    if (!isset($_POST['edit'])) {
    $title = 'Edit Tempat Magang';
        $id = $_GET['id'];
        $query = $connection->prepare('SELECT * FROM tempat_magang WHERE id = ?');
        $query->execute([$id]);
        $magang = $query->fetch(PDO::FETCH_ASSOC);
        $dataMagang = $connection->query('SELECT * FROM tempat_magang');

        require './views/jadwal/edit.view.php';
    }else {
        $id = $_GET['id'];
        $input = $_POST;
        $query = $connection->prepare("UPDATE jadwal_magang SET mahasiswa_id = ?, prodi_mhs = ?, jadwal = ?, tempat_id = ?, dosen_id = ? WHERE id = ?");
        $query->execute([$input["mahasiswa_id"], $input["prodi_mhs"], $input["jadwal"], $input["tempat_id"], $input["dosen_id"], $id]);
        header('location: /jadwal_magang');
    }

}else {
    $title = "Data Jadwal Magang";

    // Menampilkan list jadwal magang
    if (isset($_GET['keyword'])) {
        $keyword = $_GET['keyword'];

        $query = $connection->prepare('SELECT COUNT(*) as total_data FROM jadwal_magang.*,
        mahasiswa.nama as nama_mahasiswa,
        prodi.nama as nama_prodi,
        tempat_magang.namaTempat as nama_tempat,
        dosen.nama as nama_dosen, dosen.gelarDepan as gelar_depan, dosen.gelarBelakang as gelar_belakang
        FROM jadwal_magang
        JOIN mahasiswa ON mahasiswa.id = jadwal_magang.mahasiswa_id
        JOIN tempat_magang ON tempat_magang.id = jadwal_magang.tempat_id
        JOIN dosen ON dosen.id = jadwal_magang.dosen_id
        JOIN prodi ON prodi.id = jadwal_magang.prodi_mhs WHERE nama LIKE "%' . $keyword . '%"');
        $query->execute();
        $dataMagang = $query->fetchAll(PDO::FETCH_ASSOC);
    } else {
        $query = $connection->prepare("SELECT jadwal_magang.*, mahasiswa.nama as nama_mahasiswa, 
            prodi.nama as nama_prodi, 
            tempat_magang.namaTempat as nama_tempat, 
            dosen.nama as nama_dosen, 
            dosen.gelarDepan as gelar_depan, dosen.gelarBelakang as gelar_belakang 
            FROM jadwal_magang 
            JOIN mahasiswa ON mahasiswa.id = jadwal_magang.mahasiswa_id
            JOIN tempat_magang ON tempat_magang.id = jadwal_magang.tempat_id
            JOIN dosen ON dosen.id = jadwal_magang.dosen_id
            JOIN prodi ON prodi.id = jadwal_magang.prodi_mhs");
    
        $query->execute();
        $dataMagang = $query->fetchAll(PDO::FETCH_ASSOC);
    }
    

    require './views/jadwal/index.view.php';
}
