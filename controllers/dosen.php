<?php

$connection = new PDO("mysql:host=localhost;port=3306;dbname=magang;user=root;charset=utf8mb4;");

if(!isset($_SESSION['sudah_login'])) {
    header('Location: /');
}

$act = '';
$list_prodi = $connection->query('SELECT * FROM prodi');
if (isset($_GET['act'])) {
    $act = $_GET['act'];
}

    $data_per_halaman = 3;

    $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
    
    $batas_awal = ($current_page - 1) * $data_per_halaman;
    $batas_akhir = $batas_awal + $data_per_halaman - 1;
    $query = $connection->prepare("SELECT * FROM dosen LIMIT $batas_awal, $data_per_halaman");
    $query->execute();
    $data_dosen = $query->fetchAll(PDO::FETCH_ASSOC);
    $query_total = $connection->query("SELECT COUNT(*) AS total_data FROM dosen");
    $result_total = $query_total->fetch(PDO::FETCH_ASSOC);
    $total_data = $result_total['total_data'];
    $total_halaman = ceil($total_data / $data_per_halaman);


if ($act == 'del') {
    $id = $_GET['id'];
    $query = $connection->prepare("DELETE FROM dosen WHERE id = ?");
    $query->execute([$id]);

    header('location: /dosen');

}elseif (isset($_GET['act']) && $_GET['act'] == 'create') {
    // menampilkan form input
    $title = "Create dosen";
    
    $list_prodi = $connection->query('SELECT * FROM prodi');
    if ($_POST) {
        $input = $_POST;
        $query = $connection->prepare("INSERT INTO dosen (nik, nama, gelarDepan, gelarBelakang, prodi_id) VALUES (?,?,?,?,?)");
        $query->execute([$input["nik"], $input["nama"],$input["gelarDepan"], $input["gelarBelakang"], $input["prodi_id"],]);

        header('location: /dosen');
    }
    require './views/dosen/create.view.php';
}elseif (isset($_GET['act']) && $_GET['act'] == 'store') {
    // menyimpan data
    // di redirect ke list / index
    
}elseif (isset($_GET['act']) && $_GET['act'] == 'edit') {
    // menampilkan form edit
    if (!isset($_POST['edit'])) {
    $title = 'Edit dosen';
        $id = $_GET['id'];
        $query = $connection->prepare('SELECT * FROM dosen WHERE id = ?');
        $query->execute([$id]);
        $dosen = $query->fetch(PDO::FETCH_ASSOC);
        $list_prodi = $connection->query('SELECT * FROM prodi');

        require './views/dosen/edit.view.php';
    }else {
        $id = $_GET['id'];
        $input = $_POST;
        $query = $connection->prepare("UPDATE dosen SET nik = ?, nama = ?, gelarDepan = ?, gelarBelakang = ?, prodi_id = ? WHERE id = ?");
        $query->execute([$input["nik"], $input["nama"], $input["gelarDepan"], $input["gelarBelakang"], $input["prodi_id"], $id]);
        header('location: /dosen');
    }

}else {
    $title = "Data dosen";
    // Menampilkan list dosen
    if (isset($_GET['keyword'])) {
        $keyword = $_GET['keyword'];
        $query = $connection->prepare('SELECT * FROM dosen.*, prodi.nama as nama_prodi FROM dosen JOIN prodi ON prodi.id = dosen.prodi_id WHERE nama LIKE "%' . $keyword . '%"');
        $query->execute();
        $dataDosen = $query->fetchAll(PDO::FETCH_ASSOC);
    } else {
        $query = $connection->prepare("SELECT dosen.*, prodi.nama as nama_prodi FROM dosen JOIN prodi ON prodi.id = dosen.prodi_id");
        $query->execute();
        $dataDosen = $query->fetchAll(PDO::FETCH_ASSOC);
    }
    

    require './views/dosen/index.view.php';
}
