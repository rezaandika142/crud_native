<?php

$connection = new PDO("mysql:host=localhost;port=3306;dbname=magang;user=root;charset=utf8mb4;");

if(!isset($_SESSION['sudah_login'])) {
    header('Location: /');
}

$act = '';
$list_prodi = $connection->query('SELECT * FROM prodi');
if (isset($_GET['act'])) {
    $act = $_GET['act'];
}

    $data_per_halaman = 3;

    $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
    
    $batas_awal = ($current_page - 1) * $data_per_halaman;
    $batas_akhir = $batas_awal + $data_per_halaman - 1;
    $query = $connection->prepare("SELECT * FROM prodi LIMIT $batas_awal, $data_per_halaman");
    $query->execute();
    $data_dosen = $query->fetchAll(PDO::FETCH_ASSOC);
    $query_total = $connection->query("SELECT COUNT(*) AS total_data FROM prodi");
    $result_total = $query_total->fetch(PDO::FETCH_ASSOC);
    $total_data = $result_total['total_data'];
    $total_halaman = ceil($total_data / $data_per_halaman);

if ($act) {
    if ($act == 'list') {
        $query = $connection->prepare('SELECT * FROM prodi');
        $query->execute();
        $list_prodi = $query->fetchAll(PDO::FETCH_ASSOC);
        require './views/prodi/list.view.php';
    } elseif ($act == 'create') {
        $title = "Create Prodi";
        if ($_POST) {
            $input = $_POST;
            $query = $connection->prepare("INSERT INTO prodi (nama) VALUES (?)");
            $query->execute([$input["nama"]]);
            header('location: /prodi?act=list');
        }
        require './views/prodi/create.view.php';
    } elseif (isset($_GET['act']) && $_GET['act'] == 'edit') {
        // menampilkan form edit
        if (!isset($_POST['edit'])) {
        $title = 'Edit Prodi';
            $id = $_GET['id'];
            $query = $connection->prepare('SELECT * FROM prodi WHERE id = ?');
            $query->execute([$id]);
            $prodi = $query->fetch(PDO::FETCH_ASSOC);
    
            require './views/prodi/edit.view.php';
        }else {
            $id = $_GET['id'];
            $input = $_POST;
            $query = $connection->prepare("UPDATE prodi SET nama = ? WHERE id = ?");
            $query->execute([$input["nama"],$id]);
            header('location: /prodi?act=list');
        }
    
    } elseif ($act == 'delete') {
        $id = $_GET['id'];
        $query = $connection->prepare("DELETE FROM prodi WHERE id = ?");
        $query->execute([$id]);
    
        header('location: /prodi?act=list');
    
    }
    else {
        $query = $connection->query('SELECT * FROM prodi');
        $listMahasiswa = $query->fetchAll(PDO::FETCH_ASSOC);
    }
}
