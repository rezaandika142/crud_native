<?php

$connection = new PDO("mysql:host=localhost;port=3306;dbname=magang;user=root;charset=utf8mb4;");

if(!isset($_SESSION['sudah_login'])) {
    header('Location: /');
}

$act = '';
$list_prodi = $connection->query('SELECT * FROM prodi');
if (isset($_GET['act'])) {
    $act = $_GET['act'];
}


    $data_per_halaman = 3;

    $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
    
    $batas_awal = ($current_page - 1) * $data_per_halaman;
    $batas_akhir = $batas_awal + $data_per_halaman - 1;
    $query = $connection->prepare("SELECT * FROM mahasiswa LIMIT $batas_awal, $data_per_halaman");
    $query->execute();
    $data_dosen = $query->fetchAll(PDO::FETCH_ASSOC);
    $query_total = $connection->query("SELECT COUNT(*) AS total_data FROM mahasiswa");
    $result_total = $query_total->fetch(PDO::FETCH_ASSOC);
    $total_data = $result_total['total_data'];
    $total_halaman = ceil($total_data / $data_per_halaman);

if ($act == 'del') {
    $id = $_GET['id'];
    $query = $connection->prepare("DELETE FROM mahasiswa WHERE id = ?");
    $query->execute([$id]);

    header('location: /mahasiswa');

}elseif (isset($_GET['act']) && $_GET['act'] == 'create') {
    // menampilkan form input
    $title = "Create Mahasiswa";
    
    $list_prodi = $connection->query('SELECT * FROM prodi');
    if ($_POST) {
        $input = $_POST;
        $query = $connection->prepare("INSERT INTO mahasiswa (nim, nama, prodi_id, semester) VALUES (?,?,?,?)");
        $query->execute([$input["nim"], $input["nama"], $input["prodi_id"], $input["semester"]]);

        header('location: /mahasiswa');
    }
    require './views/mahasiswa/create.view.php';
}elseif (isset($_GET['act']) && $_GET['act'] == 'store') {
    // menyimpan data
    // di redirect ke list / index
    
}elseif (isset($_GET['act']) && $_GET['act'] == 'edit') {
    // menampilkan form edit
    if (!isset($_POST['edit'])) {
    $title = 'Edit Mahasiswa';
        $id = $_GET['id'];
        $query = $connection->prepare('SELECT * FROM mahasiswa WHERE id = ?');
        $query->execute([$id]);
        $mahasiswa = $query->fetch(PDO::FETCH_ASSOC);
        $list_prodi = $connection->query('SELECT * FROM prodi');

        require './views/mahasiswa/edit.view.php';
    }else {
        $id = $_GET['id'];
        $input = $_POST;
        $query = $connection->prepare("UPDATE mahasiswa SET nim = ?, nama = ?, prodi_id = ?, semester = ? WHERE id = ?");
        $query->execute([$input["nim"], $input["nama"], $input["prodi_id"], $input["semester"], $id]);
        header('location: /mahasiswa');
    }

}else {
    $title = "Data Mahasiswa";

    // Menampilkan list mahasiswa
    if (isset($_GET['keyword'])) {
        $keyword = $_GET['keyword'];
        $query = $connection->prepare('SELECT * FROM mahasiswa.*, prodi.nama as nama_prodi FROM mahasiswa JOIN prodi ON prodi.id = mahasiswa.prodi_id WHERE nama LIKE "%' . $keyword . '%"');
        $query->execute();
        $dataMahasiswa = $query->fetchAll(PDO::FETCH_ASSOC);
    } else {
        $query = $connection->prepare("SELECT mahasiswa.*, prodi.nama as nama_prodi FROM mahasiswa JOIN prodi ON prodi.id = mahasiswa.prodi_id");
        $query->execute();
        $dataMahasiswa = $query->fetchAll(PDO::FETCH_ASSOC);
    }
    

    require './views/mahasiswa/index.view.php';
}
